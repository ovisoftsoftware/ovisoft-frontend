import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContacSmallComponent } from './contac-small.component';

describe('ContacSmallComponent', () => {
  let component: ContacSmallComponent;
  let fixture: ComponentFixture<ContacSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContacSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContacSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
