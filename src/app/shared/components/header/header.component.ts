import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Header } from 'src/app/core/model/header.model';
import { HeaderService } from 'src/app/core/services/header/header.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  url: string = 'https://images.unsplash.com/photo-1599707367072-cd6ada2bc375?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1333&q=80'
  logo: Header | any;
  banner: Header | any;
  statusUpload: boolean = false;
  uploadPercent!: Observable<number | undefined>;
  imageSrc!: Observable<string | undefined>;
  logoForm!: FormGroup;
  bannerForm!: FormGroup;
  constructor(
    private headerService: HeaderService,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.getHeaderContent();
  }

  buildForm(): void {
    this.logoForm = this.formBuilder.group({
      id: [''],
      type: [''],
      image: [''],
    });

    this.bannerForm = this.formBuilder.group({
      id: [''],
      type: [''],
      image: [''],
    });
  }
  getHeaderContent(): void {
    this.headerService.getHeaderContent().subscribe(
      (res: Header[]) => {
        // console.log(res);
        this.logo = res.find(element => {
          return element.type === 'logo'
        })
        this.banner = res.find(element => {
          return element.type === 'banner'
        })
        // console.log(this.logo);
        // console.log(this.banner);
        this.logoForm.patchValue(this.logo);
        this.bannerForm.patchValue(this.banner);
      }
    );
    
  }


  editHeader(option: string): void {
    let data: Header;
    if (option === 'logo') {
      data = this.logoForm.value;
    } else {
      data = this.bannerForm.value;
    }
    this.headerService.editHeader(data)
    .then(
      (res: any) => {

      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }

  uploadFile(event: any, option: string) {
    this.statusUpload = true;
    const file = event.target.files[0];
    let dir = '';
    if (option === 'logo') {
      dir = 'header/' + this.logo.id;
    } else {
      dir = 'header/' + this.banner.id;
    }
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.imageSrc.subscribe(url => {
          if (option === 'logo') {
            this.logoForm.controls.image.setValue(url);
          } else {
            this.bannerForm.controls.image.setValue(url);
          }
          this.editHeader(option);
          this.openSnackBar();
        });
      })
    ).subscribe(() => {
    });
  }

  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }


  logout(): void {
    
  }
}
