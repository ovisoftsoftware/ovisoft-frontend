import { AngularMaterialModule } from './agular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContacSmallComponent } from './components/contac-small/contac-small.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ContacSmallComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ContacSmallComponent,
    AngularMaterialModule
  ]
})
export class SharedModule { }
