import { AboutDetailComponent } from './components/about-detail/about-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutLayoutComponent } from './components/about-layout/about-layout.component';

const routes: Routes = [
  { path: '', component: AboutLayoutComponent },
  { path: 'about-detail/:id', component: AboutDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutUsRoutingModule { }
