import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit , Inject } from '@angular/core';
//import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AboutUsService } from 'src/app/core/services/about-us/about-us.service';
@Component({
  selector: 'app-about-detail',
  templateUrl: './about-detail.component.html',
  styleUrls: ['./about-detail.component.scss']
})
export class AboutDetailComponent implements OnInit {
  urlImg = 'https://img.freepik.com/vector-gratis/fondo-numeros-datos-digitales-codigo-binario_1017-30367.jpg?size=626&ext=jpg';
  isDescription: boolean = false;
  isImage: boolean = false;
  isTitle: boolean = false;
  aboutUsDetailData: any;
  formAboutDetail!: FormGroup;

  constructor(
    private aboutUsService: AboutUsService,
    private formBuilder: FormBuilder 
  ) { 
    this.buildForm();
  }
    /*
    closeDialogAboutUsDetailComponent(): void {
      this.dialogRef.close();
    }*/

  ngOnInit(): void {
    this.getAboutUsDetail();
  }
  private buildForm(){
    this.formAboutDetail = this.formBuilder.group({
      description: [''],
      image: ['']
    })
  }
  setIsHiden(option: string): void {

    switch (option) {
      // case 'title':
      //   if (this.isTitle) {
      //     this.isTitle = false;
      //   } else {
      //     this.isTitle = true;
      //   }
      // break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
      break;
      case 'image':
        if (this.isImage) {
          this.isImage = false;
        } else {
          this.isImage = true;
        }
      break;
    }
  }

  getAboutUsDetail(){
    this.aboutUsService.getAboutUSDetail(1)
    .subscribe(
      (res:any)=>{
        this.aboutUsDetailData = res.data;
        this.formAboutDetail.patchValue(this.aboutUsDetailData);
      }
    )
  }

  editAboutUsDetail(option: string){
    switch (option) {
      case 'description':
        this.isDescription = false;
      break;
      case 'image':
        this.isImage = false;
      break;
    }
    this.aboutUsService.updateAboutDetail(this.aboutUsDetailData.id ,this.formAboutDetail.value)
    .subscribe(
      (res:any)=>{
        this.getAboutUsDetail();
      }
    )
  }

 

}
