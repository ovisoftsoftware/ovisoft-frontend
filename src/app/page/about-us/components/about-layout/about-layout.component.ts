import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AboutUsService } from 'src/app/core/services/about-us/about-us.service';
import { Company } from 'src/app/core/model/company.model';

@Component({
  selector: 'app-about-layout',
  templateUrl: './about-layout.component.html',
  styleUrls: ['./about-layout.component.scss']
})
export class AboutLayoutComponent implements OnInit {
  

  aboutUsData!: Company[];
  state: boolean = false;

  constructor(
    private formBuilder : FormBuilder,
    private aboutUsService: AboutUsService
  ) {
  }

  ngOnInit(): void {
    this.getAboutUs();
  }
  

  public getAboutUs(){
    this.state = true;
    this.aboutUsService.getAboutUsData()
    .subscribe(
      (res: any)=>{
        this.state = false;
        this.aboutUsData = res.data;
      }
    )
  } 
}
