import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerAboutComponent } from './container-about.container';

describe('ContainerAboutComponent', () => {
  let component: ContainerAboutComponent;
  let fixture: ComponentFixture<ContainerAboutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContainerAboutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
