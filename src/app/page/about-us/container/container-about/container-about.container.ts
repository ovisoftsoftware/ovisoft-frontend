import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AboutUsService } from 'src/app/core/services/about-us/about-us.service';
import { Company } from 'src/app/core/model/company.model';
import { MatDialog } from '@angular/material/dialog';
import { AboutDetailComponent } from '../../components/about-detail/about-detail.component';
@Component({
  selector: 'app-container-about',
  templateUrl: './container-about.container.html',
  styleUrls: ['./container-about.container.scss']
})
export class ContainerAboutComponent implements OnInit {
  aboutUsForm!: FormGroup;
  

  isTitle: boolean=false;
  isDescription: boolean = false;
  
  @Input() item!: Company;
  @Input() index!:number;

  @Output() evenEmiiter: EventEmitter<any> = new EventEmitter();
  constructor(
    
    private formBuilder : FormBuilder,
    private aboutUsService: AboutUsService,
    public dialog: MatDialog,

  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.aboutUsForm.patchValue(this.item);
  }
  
  openDialogAboutUsDetail():void
  {
    const dialogRef = this.dialog.open(AboutDetailComponent,
      { 
        maxWidth: '100vw',
        maxHeight: '100vh',
        width:'100%',
        height: '100%',
      });
  }

  
  private buildForm(){
    this.aboutUsForm = this.formBuilder.group({
      title: [''],
      description: ['']
    });
  }

  
  setIsHiden(option: string): void{
    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
      break;
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
      break;
    }
  }
  
  updateAboutUs(option: string): void{
    switch (option) {
      case 'title':
        this.isTitle = false;
      break;
      case 'description':
        this.isDescription = false;
      break;
    }
    this.aboutUsService.updateAboutUS(this.item.id, this.aboutUsForm.value)
    .subscribe(
      (res: Company)=>
      {
        this.evenEmiiter.emit(true);
      }
    )
  }
}
