import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogLayoutComponent } from './components/blog-layout/blog-layout.component';
import { BlogDetailComponent } from './components/blog-detail/blog-detail.component';
import { BlogContainer } from './container/blog/blog.container';
import { AngularMaterialModule } from 'src/app/shared/agular-material/angular-material.module';


@NgModule({
  declarations: [
    BlogLayoutComponent,
    BlogDetailComponent,
    BlogContainer
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    AngularMaterialModule
  ]
})
export class BlogModule { }
