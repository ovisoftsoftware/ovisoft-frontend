import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRoutingModule } from './page-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { AngularMaterialModule } from '../shared/agular-material/angular-material.module';
import { SharedModule } from '../shared/shared.module';


import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    AngularMaterialModule,
    SharedModule
  ]
})
export class PageModule { }
