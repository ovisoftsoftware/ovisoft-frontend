import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DownloadLayoutComponent } from './components/download-layout/download-layout.component';

const routes: Routes = [
  { path: '', component: DownloadLayoutComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DownloadRoutingModule { }
