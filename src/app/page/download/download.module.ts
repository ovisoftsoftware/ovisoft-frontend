import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DownloadRoutingModule } from './download-routing.module';
import { DownloadLayoutComponent } from './components/download-layout/download-layout.component';
import { DownloadDetailComponent } from './components/download-detail/download-detail.component';
import { DownloadContainer } from './container/download/download.container';
import { AngularMaterialModule } from 'src/app/shared/agular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxDocViewerModule } from 'ngx-doc-viewer';


@NgModule({
  declarations: [
    DownloadLayoutComponent,
    DownloadDetailComponent,
    DownloadContainer
  ],
  imports: [
    CommonModule,
    DownloadRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    NgxDocViewerModule
  ]
})
export class DownloadModule { }
