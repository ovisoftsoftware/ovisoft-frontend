import { Component, Input, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { File } from 'src/app/core/model/download.model';
import { DocumentService } from 'src/app/core/services/document/document.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-download',
  templateUrl: './download.container.html',
  styleUrls: ['./download.container.scss']
})
export class DownloadContainer implements OnInit {
  @Input() data!: File;
  documentForm!: FormGroup;

  isTitle: boolean = false;
  isSubtitle: boolean = false;
  isPrice: boolean = false;

  uploadPercent!: Observable<number | undefined>;
  downloadURL!: Observable<string>;
  statusUpload: boolean = false;
  imageSrc!: Observable<string | null>;
  constructor(
    private documentService: DocumentService,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
  ) {
    this.buildForm();
  }
  
  ngOnInit(): void {
    this.documentForm.patchValue(this.data);
  }
  setIsHiden(option: string): void {

    switch (option) {
      case 'title':
        if (this.isTitle) {
          this.isTitle = false;
        } else {
          this.isTitle = true;
        }
      break;
      case 'subTitle':
        if (this.isSubtitle) {
          this.isSubtitle = false;
        } else {
          this.isSubtitle = true;
        }
      break;
      case 'price':
        if (this.isPrice) {
          this.isPrice = false;
        } else {
          this.isPrice = true;
        }
      break;
      default:
        this.isPrice = false;
        this.isSubtitle = false;
        this.isTitle = false;
      break;
    }
  }

  buildForm(): void {
    this.documentForm = this.formBuilder.group({
      title: [''],
      subtitle: [''],
      image: [''],
      url: [''],
      description: [''],
      date: ['']
    })
  }

  deleteDocument(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: '¿Estas seguro de eliminar el documento:' + this.data.title + '?',
      text: "Una vez eliminado los datos no se podran recuperar",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Elimnar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
      }).then((result) => {
      if (result.isConfirmed) {
        this.documentService.deleteDocument(this.data)
        .then(
          (res: any) => {
            console.log(res);
            swalWithBootstrapButtons.fire(
              'Elimando!',
              'El documento se eliminó correctamente',
              'success'
            )
          }
        )
        .catch(
          (error: any) => {
            console.log(error);
          }
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'El documento no se eliminó',
          'error'
        )
      }
    })
  }

  editDocument(option: string): void {

    this.setIsHiden(option);
    // console.log(this.documentForm.value);
    this.documentService.editDocument(this.documentForm.value, this.data.id)
    .then(
      (res: any) => {
        // console.log(res);
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }

  uploadFile(event: any) {
    this.statusUpload = true;
    const file = event.target.files[0];
    const dir = 'documents/' + this.data.id;
    const fileRef = this.storage.ref(dir);
    const task = this.storage.upload(dir, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges()
    .pipe(
      finalize(() => {
        this.statusUpload = false;
        this.imageSrc = fileRef.getDownloadURL();
        this.imageSrc.subscribe(url => {
          this.documentForm.controls.url.setValue(url);
          this.openSnackBar();
          this.editDocument('image');
        });
      })
    ).subscribe(() => {
    });
  }

  openSnackBar(): void {
    this.snackBar.open('La imagen se subio correctamente', 'Done', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
}
