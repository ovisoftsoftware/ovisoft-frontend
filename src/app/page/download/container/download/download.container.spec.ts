import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadContainer } from './download.container';

describe('DownloadComponent', () => {
  let component: DownloadContainer;
  let fixture: ComponentFixture<DownloadContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
