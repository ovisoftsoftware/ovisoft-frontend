import { Component, OnInit } from '@angular/core';

export interface Link {
  name: string;
  rooterLink: string
}
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  links: Link[] = [
    {
      name: 'Home',
      rooterLink: 'home'
    },
    {
      name: 'Quienes Somos',
      rooterLink: 'about-us'
    },
    {
      name: 'Productos',
      rooterLink: 'product'
    },
    {
      name: 'Descargas',
      rooterLink: 'download'
    },
    {
      name: 'Contacto',
      rooterLink: 'contact'
    },
  ];
  activeLink!: Link;
  constructor() {
    if (localStorage.getItem('index')) {
      const index: number | any = localStorage.getItem('index');
      this.activeLink = this.links[+index];
    } else {
      this.activeLink = this.links[0];
    }
  }

  ngOnInit(): void {
  }

  changeLink(link: any, index: number): void {
    // console.log(link, index);
    this.activeLink = link;
    localStorage.setItem('index', '' + index);
  }

}
