import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductLayoutComponent } from './components/product-layout/product-layout.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductContainer } from './container/product/product.container';
import { SharedModule } from 'src/app/shared/shared.module';
import { AngularMaterialModule } from 'src/app/shared/agular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProductLayoutComponent,
    ProductDetailComponent,
    ProductContainer
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ]
})
export class ProductModule { }
