import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/model/product.model';
import { ProductService } from 'src/app/core/services/product/product.service';

@Component({
  selector: 'app-product-layout',
  templateUrl: './product-layout.component.html',
  styleUrls: ['./product-layout.component.scss']
})
export class ProductLayoutComponent implements OnInit {
  products!: Product[];
  state: boolean = false;
  constructor(
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.state = true;
    this.productService.getProducts().subscribe(
      (res: Product[]) => {
        this.state = false;
        console.log("resProduct",res);
        this.products = res;
      },
      (error: any) => {
        this.state = false;
        console.log(error);
      },
    );
  }

  addProduct(): void {
    const product: Product | any = {
      title: 'Nuevo Producto',
      subtitle: 'Subtitulo',
      image: 'https://images.unsplash.com/photo-1543294001-f7cd5d7fb516?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80',
      price: '10',
      description: 'Descripción',
      date: new Date,
    };
    this.productService.addProduct(product)
    .then(
      (res: any) => {
        console.log(res);
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    )
  }

}
