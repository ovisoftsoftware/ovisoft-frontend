import { Company } from 'src/app/core/model/company.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HomeService } from 'src/app/core/services/home/home.service';



@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {
  dataHome!: any;
  formHomeData!:FormGroup;
  data!: Company;
  isDescription: boolean = false;
  isVideo: boolean = false;

  editFormHome!: FormGroup;
  state: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private homeService: HomeService
    ) {
      this.buildForm();
     }

  ngOnInit(): void {
    this.getHomeDescription();
  }

  // isRolAdminLocalStorage(){
  //   if(localStorage.getItem('rol') === "1")
  //   {
  //     return true;
      
  //   }else{
  //     return false;
  //   }
  // }

  getHomeDescription(){
    this.state = true;
    this.homeService.getHome()
    .subscribe((res: any) =>
    {
        this.state = false;
        this.dataHome = res.data;
        this.data = this.dataHome.find((x: Company) => x.id === 1);
        this.formHomeData.patchValue(this.data); 
      }
    ) 
  }

  private buildForm(){
    this.formHomeData = this.formBuilder.group({
      description: [''],
      video:[''],
    })
  }
  
  setIsHiden(option: string): void {

    switch (option) {
      case 'description':
        if (this.isDescription) {
          this.isDescription = false;
        } else {
          this.isDescription = true;
        }
      break;
      case 'video':
        if (this.isVideo) {
          this.isVideo = false;
        } else {
          this.isVideo = true;
        }
      break;
    }
  }

  editDescriptionHome(option: string): void {
    switch (option) {
      case 'description':
        this.isDescription = false;
      break;
      case 'video':
        this.isVideo = false;
      break;
    }
    this.homeService.updateHome(this.data.id, this.formHomeData.value)
    .subscribe(
      (res: any) => {
        this.getHomeDescription();
      }
    )
   }
}
