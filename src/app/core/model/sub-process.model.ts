// ejemplos de modelos

export interface SubProcess {
  id: number,
  position: number,
  name: string
}