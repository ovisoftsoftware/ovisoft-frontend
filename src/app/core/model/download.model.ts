export interface File {
  id: string;
  title: string;
  subtitle: string;
  url: string;
  image: string;
  description: string;
  date: string;
}