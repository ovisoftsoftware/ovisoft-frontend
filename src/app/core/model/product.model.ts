export interface Product {
  id: string;
  title: string;
  subtitle: string;
  image: string;
  description: string;
  price: string;
  date: Date
}