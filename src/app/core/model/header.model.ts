export interface Header {
  id: string;
  image: string;
  type: string;
}