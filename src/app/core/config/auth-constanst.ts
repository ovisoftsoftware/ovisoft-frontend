export class AuthConstants {
  public static readonly AUTH = 'authToken';
  public static readonly TITLE_NAV = 'title_nav';
  public static readonly USER = 'user';
  public static readonly ROL = 'rol';
  public static readonly IDPROCESS = 'idProcessGroup';
}
