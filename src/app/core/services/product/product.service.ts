import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../../model/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private fireStoreService: AngularFirestore
  ) { }
  
  getProducts(): Observable<Product[]> {
    return this.fireStoreService
    .collection<Product>('product', ref => ref.orderBy('date', 'desc'))
    // .collection<Product>('products', ref => ref.orderBy('date', 'desc'))
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Product[]) => {
           return res;
        }
      )
    );
  }

  addProduct(product: Product): Promise<any>{
    return this.fireStoreService.collection<Product>('product').add(product);
  }

  deleteProduct(product: Product): Promise<any>{
    return this.fireStoreService.collection<Product>('product').doc(product.id).delete();
  }
  
  editProduct(product: Product, id: string): Promise<any>{
    return this.fireStoreService.collection<Product>('product').doc(id).update(product);
  }
}
