import { Company } from 'src/app/core/model/company.model';
import { map } from 'rxjs/operators';
import { HttpService } from './../http/http.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CompanyDetail } from '../../model/company-detail';


@Injectable({
  providedIn: 'root'
})
export class AboutUsService {
  constructor(
    private httpService: HttpService
  ) { }


  getAboutUsData(): Observable<Company[]>{
    return this.httpService.get('/about-us')
    .pipe(
      map(
        (res: Company[])=>{
          return res;
        })
    ); 
  }
  updateAboutUS(id: number , data: Company){
    return this.httpService.put(`/about-us/${id}`, data)
  }

  getAboutUSDetail(id: number): Observable<Response[]>{
    return this.httpService.get(`/company-detail/${id}`)
    .pipe(
      map(
        (res: Response[])=>{
          return res;
        }
      )
    )
  }

  updateAboutDetail(id: number , data: CompanyDetail[]){
    return this.httpService.put(`/company-detail/${id}`,data)
  }

}
