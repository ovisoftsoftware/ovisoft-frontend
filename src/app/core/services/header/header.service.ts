import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Header } from '../../model/header.model';


@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(
    private fireStoreService: AngularFirestore
  ) { }

  getHeaderContent(): Observable<Header[]> {
    return this.fireStoreService
    .collection<Header>('header')
    // .collection<Product>('products', ref => ref.orderBy('date', 'desc'))
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: Header[]) => {
           return res;
        }
      )
    );
  }

  editHeader(header: Header): Promise<any>{
    return this.fireStoreService.collection<Header>('header').doc(header.id).update(header);
  }
}
