import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthConstants } from '../../config/auth-constanst';
import { User } from '../../model/user.model';
import { HttpService } from '../http/http.service';
import { StorageService } from '../storage/storage.service';
import { AuthUser } from '../../model/auth.model';

import { Response } from '../../model/response.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router,
    
  ) {  }

  login(data: AuthUser): Observable<any>{
    return this.httpService.post('/login', data)
    .pipe(
      catchError(
        this.handleError
      ),
      map(
        (res: Response) => {
          
          const token: string = res.data.token;
          const dataUser: User = res.data.user;
          const rolUser:string = res.data.user.role_id;
          
          this.storageService.store(AuthConstants.TITLE_NAV, 'Home');
          this.storageService.store(AuthConstants.AUTH, token);
          this.storageService.store(AuthConstants.USER, dataUser);
          localStorage.setItem('rol',rolUser);
         
          this.router.navigate(['ovisoft']);
          return "Bienvenido";
        },
      )
    );
  }
  
  logout(id: string): Observable<any>{
    return this.httpService.post('/logout', id)
  }

  private handleError(httpResponse: HttpErrorResponse) {
    const error = httpResponse.error;
     return throwError(error);
  }
}
