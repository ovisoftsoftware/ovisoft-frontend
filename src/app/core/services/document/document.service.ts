import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { File } from '../../model/download.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(
    private fireStoreService: AngularFirestore
  ) { }
  
  getDocuments(): Observable<File[]> {
    return this.fireStoreService
    .collection<File>('document', ref => ref.orderBy('date', 'desc'))
    // .collection<Product>('products', ref => ref.orderBy('date', 'desc'))
    .valueChanges({ idField: 'id' })
    .pipe(
      map(
        (res: File[]) => {
          // console.log(res);
          return res;
        }
      )
    );
  }

  addDocument(document: File): Promise<any>{
    return this.fireStoreService.collection<File>('document').add(document);
  }

  deleteDocument(document: File): Promise<any>{
    return this.fireStoreService.collection<File>('document').doc(document.id).delete();
  }
  
  editDocument(document: File, id: string): Promise<any>{
    return this.fireStoreService.collection<File>('document').doc(id).update(document);
  }
}
